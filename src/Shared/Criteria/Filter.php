<?php

declare(strict_types=1);

namespace App\Shared\Criteria;

class Filter
{
    public string $field;
    public FilterOperator $operator;
    public string $value;

    public function __construct(string $field, FilterOperator $operator, string $value)
    {
        $this->field    = $field;
        $this->operator = $operator;
        $this->value    = $value;
    }

    public static function fromValues(array $values): self
    {
        return new self($values['field'], new FilterOperator($values['operator']), $values['value']);
    }
}