<?php

declare(strict_types=1);

namespace App\Shared\Criteria;

final class Criteria
{
    public Filters $filters;
    public Order $order;
    public ?int $limit;
    public ?int $offset;

    public function __construct(Filters $filters, Order $order, ?int $limit = null, ?int $offset = null)
    {
        $this->filters = $filters;
        $this->order   = $order;
        $this->limit   = $limit;
        $this->offset  = $offset;
    }

    public function hasFilters(): bool
    {
        return $this->filters->count() > 0;
    }

    public function hasOrder(): bool
    {
        return !$this->order->isNone();
    }

    public function plainFilters(): array
    {
        return $this->filters->getFilters();
    }
}