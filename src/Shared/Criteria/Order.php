<?php

declare(strict_types=1);

namespace App\Shared\Criteria;

final class Order
{
    public string $orderBy;
    public OrderType $orderType;

    public function __construct(string $orderBy, OrderType $orderType)
    {
        $this->orderBy   = $orderBy;
        $this->orderType = $orderType ?? OrderType::asc();
    }

    public static function fromValues(?string $orderBy, ?string $order): Order
    {
        return null === $orderBy
            ? self::none()
            : new Order($orderBy, new OrderType($order));
    }

    public static function none(): Order
    {
        return new Order('', OrderType::none());
    }

    public function isNone(): bool
    {
        return $this->orderType->isNone();
    }
}