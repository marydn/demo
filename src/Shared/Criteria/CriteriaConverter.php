<?php

declare(strict_types=1);

namespace App\Shared\Criteria;

use Doctrine\Common\Collections\Criteria as DoctrineCriteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\CompositeExpression;

final class CriteriaConverter
{
    public Criteria $criteria;
    public array $criteriaToDoctrineFields;

    public function __construct(Criteria $criteria, array $criteriaToDoctrineFields = [])
    {
        $this->criteria = $criteria;
        $this->criteriaToDoctrineFields = $criteriaToDoctrineFields;
    }

    public static function convert(Criteria $criteria, array $criteriaToDoctrineFields = []): DoctrineCriteria
    {
        $converter = new self($criteria, $criteriaToDoctrineFields);

        return $converter->convertToDoctrineCriteria();
    }

    private function convertToDoctrineCriteria(): DoctrineCriteria
    {
        return new DoctrineCriteria(
            $this->buildExpression($this->criteria),
            $this->formatOrder($this->criteria),
            $this->criteria->offset,
            $this->criteria->limit
        );
    }

    private function buildExpression(Criteria $criteria): ?CompositeExpression
    {
        if ($criteria->hasFilters()) {
            return new CompositeExpression(
                CompositeExpression::TYPE_AND,
                array_map($this->buildComparison(), $criteria->plainFilters())
            );
        }

        return null;
    }

    private function buildComparison(): callable
    {
        return function (Filter $filter): Comparison {
            $field = $this->mapFieldValue($filter);
            $value = $filter->value;

            return new Comparison($field, $filter->operator->value(), $value);
        };
    }

    private function mapFieldValue(Filter $field)
    {
        return array_key_exists($field->field, $this->criteriaToDoctrineFields)
            ? $this->criteriaToDoctrineFields[$field->value]
            : $field->value;
    }

    private function formatOrder(Criteria $criteria): ?array
    {
        if (!$criteria->hasOrder()) {
            return null;
        }

        return [$this->mapOrderBy($criteria->order) => $criteria->order->orderType];
    }

    private function mapOrderBy(Order $order)
    {
        return isset($this->$this->criteriaToDoctrineFields[$order->orderBy])
            ? $this->criteriaToDoctrineFields[$order->orderBy]
            : $order->orderBy;
    }
}