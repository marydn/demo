<?php

declare(strict_types=1);

namespace App\Shared\Criteria;

use App\Shared\Collection;

final class Filters extends Collection
{
    protected function getType(): string
    {
        return Filter::class;
    }

    public static function fromValues(array $values): self
    {
        return new self(array_map(self::filterBuilder(), $values));
    }

    public function add(Filter $filter): self
    {
        return new self(array_merge($this->getItems(), [$filter]));
    }

    public function getFilters(): array
    {
        return $this->getItems();
    }

    private static function filterBuilder(): callable
    {
        return function (array $values) {
            return Filter::fromValues($values);
        };
    }
}