<?php

declare(strict_types=1);

namespace App\Shared\Criteria;

final class AttributeFilter extends Filter
{
    public function __construct(string $field, FilterOperator $operator, string $value)
    {
        static::validate($field);

        parent::__construct($field, $operator, $value);
    }

    public static function validate(string &$field)
    {
        if (false === stripos($field, 'attribute.')) {
            $field = 'attribute.'.$field;
        }
    }
}