<?php

declare(strict_types=1);

namespace App\Shared\Entity;

use App\Shared\ValueObject\Uuid;

final class UserId extends Uuid
{
}