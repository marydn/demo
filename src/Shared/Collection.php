<?php

declare(strict_types=1);

namespace App\Shared;

abstract class Collection implements \Countable, \IteratorAggregate
{
    private array $items;

    abstract protected function getType(): string;

    public function __construct(array $items)
    {
        static::validateType($this->getType(), $items);

        $this->items = $items;
    }

    public function toArray(): array
    {
        return $this->items;
    }

    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this);
    }

    public function count(): int
    {
        return count($this->items);
    }

    protected function getItems(): array
    {
        return $this->items;
    }

    protected function each(callable $function)
    {
        return array_map(function($item) use ($function) {
            return call_user_func($function, $item);
        }, $this->items);
    }

    private static function validateType(string $type, $items)
    {
        $validate = function($item) use ($type) {
            if (!$item instanceof $type) {
                throw new \InvalidArgumentException(sprintf('Only %s can be added', $type));
            }
        };

        return array_map(function($item) use ($validate) {
            return call_user_func($validate, $item);
        }, $items);
    }
}