<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use App\Exception\UserNotFoundException;
use App\Shared\Criteria\Criteria;
use App\Shared\Criteria\Filter;
use App\Shared\Entity\UserId;

final class FileUserRepository implements UserRepositoryInterface
{
    private const FILE_PATH = __DIR__ . '/../../data/db/files/users/';

    /**
     * @throws UserNotFoundException
     */
    public function findOneById(UserId $id): ?User
    {
        if (!file_exists($this->fileName($id))) {
            throw new UserNotFoundException();
        }

        return unserialize(\file_get_contents($this->fileName($id)));
    }

    public function save(User $user): void
    {
        \file_put_contents($this->fileName($user->id), \serialize($user));
    }

    public function delete(UserId $id): void
    {
        @\unlink($this->fileName($id));
    }

    public function findByCriteria(Criteria $criteria): array
    {
        $users = $this->loadFiles();

        if ($criteria->hasFilters()) {
            $users = array_filter($users, function (User $user) use ($criteria) {
                return array_map(function(Filter $filter) use ($user) {
                    $field = $filter->field;

                    if ($filter->operator->isContaining()) {
                        if (stripos($user->{$field}, $filter->value) !== false) {
                            return $user;
                        }
                    }
                }, $criteria->filters->getFilters());
            });

            $users = array_slice($users, $criteria->offset ?? 0, $criteria->limit);
        }

        return $users;
    }

    private function loadFiles(): array
    {
        $directory = new \RecursiveDirectoryIterator(self::FILE_PATH, \FilesystemIterator::KEY_AS_FILENAME);
        $iterator = new \RecursiveIteratorIterator($directory);
        $regex = new \RegexIterator($iterator, '/^.+\.repository/i', \RecursiveRegexIterator::GET_MATCH);

        $files = \array_keys(\iterator_to_array($regex));

        return \array_map(function ($fileName) {
            $id = new UserId(str_replace('.repository', '', $fileName));

            return $this->findOneById($id);
        }, $files);
    }

    private function fileName(UserId $id): string
    {
        if (!is_dir(self::FILE_PATH)) {
            mkdir(self::FILE_PATH, 0700, true);
        }

        return sprintf('%s%s.repository', self::FILE_PATH, $id->value());
    }
}