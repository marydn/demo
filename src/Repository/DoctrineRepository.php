<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Setup;

abstract class DoctrineRepository
{
    private EntityManagerInterface $entityManager;

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->entityManager = EntityManager::create($connection, static::createConfiguration());
    }

    private static function createConfiguration(): Configuration
    {
        $config = Setup::createConfiguration(true, null, new ArrayCache());

        $config->setMetadataDriverImpl(new SimplifiedYamlDriver(array(
            'App\Entity'
        )));

        return $config;
    }

    protected function entityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function repository($entityClass): EntityRepository
    {
        return $this->entityManager->getRepository($entityClass);
    }

    protected function queryBuilder(): QueryBuilder
    {
        return $this->entityManager->createQueryBuilder();
    }
}