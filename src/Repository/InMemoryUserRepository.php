<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use App\Exception\UserNotFoundException;
use App\Shared\Criteria\Criteria;
use App\Shared\Criteria\Filter;
use App\Shared\Entity\UserId;

final class InMemoryUserRepository implements UserRepositoryInterface
{
    private array $memory = [];

    /**
     * @throws UserNotFoundException
     */
    public function findOneById(UserId $id): ?User
    {
        if (!isset($this->memory[$id->value()])) {
            throw new UserNotFoundException();
        }

        return $this->memory[$id->value()];
    }

    public function save(User $user): void
    {
        $this->memory[$user->id->value()] = $user;
    }

    public function delete(UserId $id): void
    {
        unset($this->memory[$id->value()]);
    }

    public function findByCriteria(Criteria $criteria): array
    {
        if ($criteria->hasFilters()) {
            $users = array_filter($this->memory, function (User $user) use ($criteria) {
                return array_map(function(Filter $filter) use ($user) {
                    $field = $filter->field;

                    if ($filter->operator->isContaining()) {
                        if (stripos($user->{$field}, $filter->value) !== false) {
                            return $user;
                        }
                    }
                }, $criteria->filters->getFilters());
            });

            $users = array_slice($users, $criteria->offset ?? 0, $criteria->limit);

            return $users;
        }

        return $this->memory;
    }
}