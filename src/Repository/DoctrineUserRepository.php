<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Attribute;
use App\Entity\User;
use App\Entity\UserAttribute;
use App\Entity\UserAttributes;
use App\Exception\UserNotFoundException;
use App\Shared\Criteria\Criteria;
use App\Shared\Criteria\CriteriaConverter;
use App\Shared\Entity\AttributeId;
use App\Shared\Entity\UserId;

final class DoctrineUserRepository extends DoctrineRepository implements UserRepositoryInterface
{
    public function findOneById(UserId $id): ?User
    {
        $user = $this->queryBuilder()
            ->select('u')
            ->from('user', 'u')
            ->where('u.id = :id')
            ->setParameter('id', $id->value())
            ->getQuery()
            ->getArrayResult();

        if (empty($user)) {
            throw new UserNotFoundException();
        }

        $attributes = $this->findAttributesByUserId($id);

        return new User(new UserId($user['id']), $user['name'], new UserAttributes($attributes));
    }

    public function findAttributesByUserId(UserId $id): array
    {
        $attributes = $this->queryBuilder()
            ->select('ua.attribute_id AS id, a.name, ua.value')
            ->from('user_attributes', 'ua')
            ->innerJoin('ua', 'attribute', 'a', 'ua.attribute_id = a.id')
            ->where('ua.user_id = :id')
            ->setParameter('id', $id->value())
            ->getQuery()
            ->getArrayResult();

        if (!$attributes) {
            return [];
        }

        return array_map(
            function ($attribute) {
                $attrId = new AttributeId($attribute['id']);
                $attr   = new Attribute($attrId, $attribute['name']);

                return new UserAttribute($attr, $attribute['value']);
            },
            $attributes
        );
    }

    public function save(User $user): void
    {
        try {
            $foundUser = $this->findOneById($user->id);
        } catch (UserNotFoundException $exception) {
            $foundUser = false;
        }

        if ($foundUser) {
            $this->update($user);

            return;
        }

        $this->insert($user);
    }

    public function delete(UserId $id): void
    {
        $this->queryBuilder()
            ->delete('user')
            ->where('id = :id')
            ->setParameter('id', $id->value())
            ->getQuery()
            ->execute();
    }

    public function findByCriteria(Criteria $criteria): array
    {
        $qb = $this->queryBuilder()
            ->select('*')
            ->from('user', 'u');

        $doctrineCriteria = CriteriaConverter::convert($criteria);
        $users            = $this->repository(User::class)->matching($doctrineCriteria)->toArray();

        return array_map(
            function ($user) {
                return new User(new UserId($user['id']), $user['name']);
            },
            $users
        );
    }

    private function update(User $user): void
    {
        $this->queryBuilder()
            ->update('user')
            ->set('name', ':name')
            ->where('id = :id')
            ->setParameters(
                [
                    'id'   => $user->id->value(),
                    'name' => $user->name,
                ]
            )
            ->execute();
    }

    private function insert(User $user): void
    {
        $this->queryBuilder()
            ->insert('user')
            ->values(
                [
                    'id'   => ':id',
                    'name' => ':name',
                ]
            )
            ->setParameters(
                [
                    'id'   => $user->id->value(),
                    'name' => $user->name,
                ]
            )
            ->execute();

        if ($user->attributes) {
            foreach ($user->attributes->getIterator() as $attribute) {

                $findAttribute = $this->findOneAttributeById($attribute->id->value());
                if (!$findAttribute) {
                    $this->insertAttribute($attribute);
                }

                $this->queryBuilder()
                    ->insert('user_attributes')
                    ->values(
                        [
                            'user_id'      => ':user_id',
                            'attribute_id' => ':attribute_id',
                            'value'        => ':value',
                        ]
                    )
                    ->setParameters(
                        [
                            'user_id'      => $user->id->value(),
                            'attribute_id' => $attribute->id,
                            'value'        => $attribute->name,
                        ]
                    )
                    ->execute();
            }
        }
    }

    private function insertAttribute(Attribute $attribute)
    {
        $this->queryBuilder()
            ->insert('attribute')
            ->values(
                [
                    'id'   => ':id',
                    'name' => ':name',
                ]
            )
            ->setParameters(
                [
                    'id'   => $attribute->id->value(),
                    'name' => $attribute->name,
                ]
            )
            ->execute();
    }

    private function findOneAttributeById(AttributeId $id): Attribute
    {
        $attribute = $this->queryBuilder()
            ->select('*')
            ->from('attribute', 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id->value())
            ->execute()
            ->fetch();

        return new Attribute($attribute['id'], $attribute['name']);
    }
}