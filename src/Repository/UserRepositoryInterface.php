<?php

namespace App\Repository;

use App\Entity\User;
use App\Exception\UserNotFoundException;
use App\Shared\Criteria\Criteria;
use App\Shared\Entity\UserId;

interface UserRepositoryInterface
{
    /**
     * @throws UserNotFoundException
     */
    public function findOneById(UserId $id): ?User;
    public function save(User $user): void;
    public function delete(UserId $id): void;
    public function findByCriteria(Criteria $criteria): array;
}