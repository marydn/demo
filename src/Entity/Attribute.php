<?php

declare(strict_types=1);

namespace App\Entity;

use App\Shared\Entity\AttributeId;

final class Attribute
{
    public AttributeId $id;
    public string $name;

    public function __construct(AttributeId $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}