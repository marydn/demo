<?php

declare(strict_types=1);

namespace App\Entity;

use App\Shared\Collection;

final class UserAttributes extends Collection
{
    public function __construct(array $items)
    {
        static::removeDuplicates($items);

        parent::__construct($items);
    }

    protected function getType(): string
    {
        return UserAttribute::class;
    }

    public static function removeDuplicates(array &$items)
    {
        $items = array_unique(array_reverse($items));
    }
}