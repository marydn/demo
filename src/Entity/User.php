<?php

declare(strict_types=1);

namespace App\Entity;

use App\Shared\Entity\UserId;

final class User
{
    public UserId $id;
    public string $name;
    public ?UserAttributes $attributes;

    public function __construct(UserId $id, string $name, ?UserAttributes $attributes = null)
    {
        $this->id         = $id;
        $this->name       = $name;
        $this->attributes = $attributes;
    }
}