<?php

declare(strict_types=1);

namespace App\Entity;

final class UserAttribute
{
    public Attribute $attribute;
    public string $value;

    public function __construct(Attribute $attribute, $value)
    {
        $this->attribute = $attribute;
        $this->value     = $value;
    }

    public function __toString()
    {
        return $this->attribute->name;
    }
}