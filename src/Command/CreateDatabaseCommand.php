<?php

declare(strict_types=1);

namespace App\Command;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateDatabaseCommand extends Command
{
    protected static $defaultName = 'app:create-db';

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;

        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Creating Database...');

        $this->createDb();

        $output->writeln('Done!');

        return 0;
    }

    private function createDb()
    {
        $sm = $this->connection->getSchemaManager();
        $schema = $sm->createSchema();
        $usersTable = $schema->createTable('user');
        $usersTable->addColumn('id', 'string');
        $usersTable->addColumn('name', 'string');
        $usersTable->setPrimaryKey(['id']);

        $attributeTable = $schema->createTable('attribute');
        $attributeTable->addColumn('id', 'string');
        $attributeTable->addColumn('name', 'string');
        $attributeTable->setPrimaryKey(['id']);
        $attributeTable->addUniqueIndex(['name']);

        $mn = $schema->createTable('user_attributes');
        $mn->addColumn('user_id', 'string');
        $mn->addColumn('attribute_id', 'string');
        $mn->addColumn('value', 'string');
        $mn->setPrimaryKey(['user_id', 'attribute_id']);
        $mn->addForeignKeyConstraint($usersTable, array('user_id'), array('id'), array('onDelete' => 'CASCADE'));
        $mn->addForeignKeyConstraint($attributeTable, array('attribute_id'), array('id'), array('onDelete' => 'CASCADE'));

        $sql = $schema->toSql($this->connection->getDatabasePlatform());

        foreach ($sql as $query) {
            $this->connection->executeQuery($query);
        }
    }
}