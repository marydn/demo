<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Repository\InMemoryUserRepository;
use App\Repository\UserRepositoryInterface;

final class InMemoryUserRepositoryTest extends UserRepositoryTest
{
    public function getRepository(): UserRepositoryInterface
    {
        return new InMemoryUserRepository();
    }
}