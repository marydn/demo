<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Repository\FileUserRepository;
use App\Repository\UserRepositoryInterface;

final class FileUserRepositoryTest extends UserRepositoryTest
{
    public function getRepository(): UserRepositoryInterface
    {
        return new FileUserRepository();
    }
}