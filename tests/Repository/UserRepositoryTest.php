<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Attribute;
use App\Entity\User;
use App\Entity\UserAttribute;
use App\Entity\UserAttributes;
use App\Exception\UserNotFoundException;
use App\Repository\UserRepositoryInterface;
use App\Shared\Criteria\AttributeFilter;
use App\Shared\Criteria\Criteria;
use App\Shared\Criteria\Filter;
use App\Shared\Criteria\FilterOperator;
use App\Shared\Criteria\Filters;
use App\Shared\Criteria\Order;
use App\Shared\Entity\AttributeId;
use App\Shared\Entity\UserId;
use PHPUnit\Framework\TestCase;

abstract class UserRepositoryTest extends TestCase
{
    abstract public function getRepository(): UserRepositoryInterface;

    public function testEmptyRepositoryThrowsException(): void
    {
        $repository = $this->getRepository();

        $this->expectException(UserNotFoundException::class);

        $repository->findOneById(UserId::random());
    }

    public function testNotEmptyRepositoryReturnsUser(): void
    {
        $repository = $this->getRepository();

        $userId = UserId::random();
        $user   = new User($userId, 'Gandalf');

        $repository->save($user);

        $findUser = $repository->findOneById($userId);

        $this->assertEquals($user, $findUser);
    }

    public function testFindNonExistentUser(): void
    {
        $repository = $this->getRepository();

        $this->expectException(UserNotFoundException::class);
        $repository->findOneById(UserId::random());
    }

    public function testSaveManyUsersAndNoMutation(): void
    {
        $repository = $this->getRepository();

        $user1 = new User(UserId::random(), 'Arwen');
        $user2 = new User(UserId::random(), 'Legolas');
        $user3 = new User(UserId::random(), 'Galadriel');

        $repository->save($user1);
        $repository->save($user2);
        $repository->save($user2);
        $repository->save($user3);

        $foundUser2 = $repository->findOneById($user2->id);

        $this->assertEquals($foundUser2->name, $user2->name);
    }

    public function testDeleteNonExistentUser(): void
    {
        $repository = $this->getRepository();

        $repository->delete(UserId::random());

        $this->assertTrue(true);
    }

    public function testDeleteExistentUserThrowsException(): void
    {
        $repository = $this->getRepository();

        $user = new User(UserId::random(), 'Galadriel');
        $repository->save($user);

        $repository->delete($user->id);

        $this->expectException(UserNotFoundException::class);
        $repository->findOneById($user->id);
    }

    public function testDeleteOneUserAndNotTheOther(): void
    {
        $repository = $this->getRepository();

        $user1 = new User(UserId::random(), 'Legolas');
        $user2 = new User(UserId::random(), 'Galadriel');

        $repository->save($user1);
        $repository->save($user2);

        $repository->delete($user1->id);

        $userFound = $repository->findOneById($user2->id);

        $this->assertEquals($user2->id, $userFound->id);
    }

    public function testFindUserByName(): void
    {
        $repository = $this->getRepository();

        $user = new User(UserId::random(), 'Misty Mountain');

        $repository->save($user);

        $filters = new Filters(
            [
                new Filter('name', new FilterOperator(FilterOperator::CONTAINS), 'Misty'),
            ]
        );

        $criteria = new Criteria($filters, Order::none());

        $usersFound = $repository->findByCriteria($criteria);

        $this->assertContainsEquals($user, $usersFound);
    }

    public function testFindUsersByAttribute(): void
    {
        $repository = $this->getRepository();

        $attr1 = new Attribute(AttributeId::random(), 'Color de ojos');
        $attr2 = new Attribute(AttributeId::random(), 'Color de camisa');
        $attr3 = new Attribute(AttributeId::random(), 'Color de casa');
        $attr4 = new Attribute(AttributeId::random(), 'Color de coche');
        $attr5 = new Attribute(AttributeId::random(), 'Número de hijos');

        $user1 = new User(UserId::random(), 'Misty Mountain', new UserAttributes([
            new UserAttribute($attr1, 'Azul claro'),
            new UserAttribute($attr2, 'Azul oscuro'),
            new UserAttribute($attr3, 'Azul oscuro'),
        ]));
        $user2 = new User(UserId::random(), 'Laura', new UserAttributes([
            new UserAttribute($attr2, 'Azul claro'),
            new UserAttribute($attr3, 'Azul oscuro'),
            new UserAttribute($attr4, 'Azul oscuro'),
        ]));
        $user3 = new User(UserId::random(), 'Jhon', new UserAttributes([
            new UserAttribute($attr1, 'Azul claro'),
            new UserAttribute($attr5, 'Azul claro'),
        ]));
        $user4 = new User(UserId::random(), 'Doe', new UserAttributes([
            new UserAttribute($attr1, 'Azul claro'),
            new UserAttribute($attr2, 'Azul oscuro'),
            new UserAttribute($attr3, 'Azul oscuro'),
        ]));
        $user5 = new User(UserId::random(), 'Pepe', new UserAttributes([
            new UserAttribute($attr1, 'Azul claro'),
            new UserAttribute($attr2, 'Azul oscuro'),
            new UserAttribute($attr3, 'Azul oscuro'),
        ]));
        $user6 = new User(UserId::random(), 'Pepe Jr');

        $repository->save($user1);
        $repository->save($user2);
        $repository->save($user3);
        $repository->save($user4);
        $repository->save($user5);
        $repository->save($user6);

        $filters = new Filters(
            [
                new Filter('name', new FilterOperator(FilterOperator::CONTAINS), 'Pepe'),
                //new AttributeFilter('attribute.name', new FilterOperator(FilterOperator::CONTAINS), 'azul'),
            ]
        );

        $criteria = new Criteria($filters, Order::none());

        $usersFound = $repository->findByCriteria($criteria);

        //$this->assertContainsEquals(array($user1, $user2, $user3, $user4, $user5), $usersFound);

        $this->assertTrue(true);
    }
}