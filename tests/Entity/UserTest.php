<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Attribute;
use App\Entity\User;
use App\Entity\UserAttribute;
use App\Entity\UserAttributes;
use App\Shared\Entity\AttributeId;
use App\Shared\Entity\UserId;
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    public function testCreateUserWithNoAttributes(): void
    {
        $userId = UserId::random();
        $user   = new User($userId, 'Jhon');

        $this->assertEquals($userId, $user->id);
    }

    public function testCreateUserWithAttributes(): void
    {
        $attr1 = new Attribute(AttributeId::random(), 'Color de ojos');
        $attr2 = new Attribute(AttributeId::random(), 'Color de camisa');
        $attr3 = new Attribute(AttributeId::random(), 'Color de casa');
        $attr4 = new Attribute(AttributeId::random(), 'Color de coche');
        $attr5 = new Attribute(AttributeId::random(), 'Número de hijos');

        $attributes = new UserAttributes(
            [
                new UserAttribute($attr1, 'Azul claro'),
                new UserAttribute($attr2, 'Azul oscuro'),
                new UserAttribute($attr3, 'Azul oscuro'),
                new UserAttribute($attr4, 'Rosa'),
                new UserAttribute($attr5, 'Tres'),
            ]
        );

        $user = new User(UserId::random(), 'Jhon', $attributes);

        $this->assertEquals($attributes, $user->attributes);
    }

    public function testNotOverwritingAttributesCollection(): void
    {
        $attr1 = new Attribute(AttributeId::random(), 'Color de ojos');
        $attr2 = new Attribute(AttributeId::random(), 'Color de camisa');
        $attr3 = new Attribute(AttributeId::random(), 'Color de casa');

        $overwrite1 = new UserAttribute($attr1, 'Azul claro');
        $overwrite2 = new UserAttribute($attr1, 'Azul oscuro');

        $attributes = new UserAttributes(
            [
                $overwrite1,
                new UserAttribute($attr2, 'Marrón'),
                new UserAttribute($attr3, 'Morado'),
                $overwrite2,
            ]
        );

        $this->assertCount(3, $attributes);
        $this->assertContains($overwrite2, $attributes->toArray());
        $this->assertNotContains($overwrite1, $attributes->toArray());
    }
}